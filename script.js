// A cambiar el sonido, mira a linea 13.
// Hay que decir la computadora a donde va y lo que va presentar.

// play instrument 1
$(function() {
	var up = true;
	$ (document).keydown(function(e) {
		if (e.keyCode == 32 && up == true) {
			document.getElementById("inst1").src="images/blue-on.png";
			up = false;
			var audio = new Audio('sounds/thump-drum.wav');
			audio.play();
		}
	})
	
    $ (document).keyup(function(e) {
        if (e.keyCode == 32) {
            document.getElementById("inst1").src="images/blue.png";
            up = true;
        }
    })
});

// play instrument 2
$(function() {
    var up = true;
    $ (document).keydown(function(e) {
        if (e.keyCode == 37 && up == true) {
            document.getElementById("inst2").src="images/red-on.png";
            up = false;
            var audio = new Audio('sounds/drum.wav');
            audio.play();
        }
    })
    
    $ (document).keyup(function(e) {
        if (e.keyCode == 37) {
            document.getElementById("inst2").src="images/red.png";
            up = true;
        }
    })
});

// play instrument 3
$(function() {
	var up = true;
	$ (document).keydown(function(e) {
		e.preventDefault();
		if (e.keyCode == 38 && up == true) {
			document.getElementById("inst3").src="images/purple-on.png";
			up = false;
			var audio = new Audio('sounds/guitar-c-chord.wav');
			audio.play();
		}
	})
	
	$ (document).keyup(function(e) {
		if (e.keyCode == 38) {
			document.getElementById("inst3").src="images/purple.png";
			up = true;
		}
	})
});

// play instrument 4
$(function() {
	var up = true;
	$ (document).keydown(function(e) {
		e.preventDefault();
		if (e.keyCode == 39 && up == true) {
			document.getElementById("inst4").src="images/orange-on.png";
			up = false;
			var audio = new Audio('sounds/tambourine.wav');
			audio.play();
		}
	})
	
	$ (document).keyup(function(e) {
		if (e.keyCode == 39) {
			document.getElementById("inst4").src="images/orange.png";
			up = true;
		}
	})
});


